from communications import *
from config import REST_API_PARAMS, ARDUINO_PARAMS, SYSTEM_PARAMS
from distutils.util import strtobool
from datetime import *

# Definimos los end-points a emplear
API_ENDPOINT_AUTH = REST_API_PARAMS["ROOT_URL"] + "/api/acceder/"
API_ENDPOINT_REFRESH = REST_API_PARAMS["ROOT_URL"] + "/api/refrescartoken/"
API_ENDPOINT_TEMPERATURA = REST_API_PARAMS["ROOT_URL"] + "/api/temperaturalist/"
API_ENDPOINT_HUMEDAD = REST_API_PARAMS["ROOT_URL"] + "/api/humedadlist/"
API_ENDPOINT_ALARMAS = REST_API_PARAMS["ROOT_URL"] + "/api/alarmalist/"
API_ENDPOINT_MODOFUNC_LAST = REST_API_PARAMS["ROOT_URL"] + "/api/modo/1"
API_ENDPOINT_IP_LAST = REST_API_PARAMS["ROOT_URL"] + "/api/ip/1"
API_ENDPOINT_IP_NOW = "https://api.ipify.org?format=json"

# COMUNICACIÓN SERIE
# Carácter de separación de parámetros en la cadena de caracteres
CARACTER_SEPARACION = ','
# Cadena de caracteres con los parámetros a enviar a Arduino por puerto serie
CARACTER_FIN_CADENA = '$'



class ModoFuncionamiento(RestApiComunicacion):

    def __init__(self):
        #self.id = 1
        self.modoid = None
        self.JSON_dict = {"modo_id": self.modoid}

    def comprobar_modo(self, modo):
        # Se comprueba si el modo actual es distinto al modo obtenido del servidor
        if self.modoid != modo:
            self.modoid = modo
            return True
        else:
            return False


    def api_get(self, token):

        peticion = super().api_get(endpoint=API_ENDPOINT_MODOFUNC_LAST,
                                   token=token)
        respuesta = peticion["respuesta"]
        if respuesta is not None:
            # Se comprueba a partir del ćodigo del resultado de la petición si ésta ha sido Satisfactoria
            if super().api_check_respuesta(respuesta) == 1:
                modo = peticion["request_dict"]["modo"]["id"]
                # Se comprueba si el modo actual es distinto al obtenido del servidor
                cambio = self.comprobar_modo(modo)
                acceder_requerido = False
            # Se comprueba a partir del ćodigo del resultado de la petición si se requiere refrescar el token JWT
            elif super().api_check_respuesta(respuesta) == 2:
                cambio = False
                acceder_requerido = True

        else:
            cambio = None
            acceder_requerido = False
        # Se retornan los valores booleanos que indican si ha habido cambio y si se necesita un refresco de token JWT
        return {"cambio": cambio, "acceder_req": acceder_requerido}


    def api_put(self, token):

        peticion = super().api_put(endpoint=API_ENDPOINT_MODOFUNC_LAST,
                                   token=token,
                                   data=self.JSON_dict)
        respuesta = peticion["respuesta"]
        if respuesta is not None:
            # Se comprueba a partir del ćodigo del resultado de la petición si ésta ha sido Satisfactoria
            if super().api_check_respuesta(respuesta) == 1:
                acceder_requerido = False
            # Se comprueba a partir del ćodigo del resultado de la petición si se requiere refrescar el token JWT
            elif super().api_check_respuesta(respuesta) == 2:
                acceder_requerido = True
        else:
            acceder_requerido = False
        # Se retorna el valor booleano que indican si se necesita un refresco de token JWT
        return {"acceder_req": acceder_requerido}

    def serial_set(self):
        # Envío del modo de funcionamiento a Arduino
        return 'M' + str(self.modoid)


class Temperatura(RestApiComunicacion):

    def __init__(self):
        self.valor = 0
        self.unidad = 1  # Por defecto ºC
        self.JSON_dict = {"valor": self.valor, "unidad_id": self.unidad}

    def api_post(self, token):

        peticion = super().api_post(endpoint=API_ENDPOINT_TEMPERATURA,
                                    token=token,
                                    data=self.JSON_dict)
        respuesta = peticion["respuesta"]
        if respuesta is not None:
            if super().api_check_respuesta(respuesta) == 1:
                acceder_requerido = False
                print("Temperatura enviada, ", self.valor)
            elif super().api_check_respuesta(respuesta) == 2:
                acceder_requerido = True
        else:
            acceder_requerido = False

        return {"acceder_req": acceder_requerido}


    def serial_get(self, valor):
        self.JSON_dict["valor"] = valor
        self.valor = valor


class Humedad(RestApiComunicacion):

    def __init__(self):
        self.valor = 0
        self.unidad = 4  # Por defecto %
        self.JSON_dict = {"valor": self.valor, "unidad_id": self.unidad}


    def api_post(self, token):

        peticion = super().api_post(endpoint=API_ENDPOINT_HUMEDAD,
                                    token=token,
                                    data=self.JSON_dict)
        respuesta = peticion["respuesta"]
        if respuesta is not None:
            if super().api_check_respuesta(respuesta) == 1:
                acceder_requerido = False
                print("Humedad enviada, ", self.valor)
            elif super().api_check_respuesta(respuesta) == 2:
                acceder_requerido = True
        else:
            acceder_requerido = False

        return {"acceder_req": acceder_requerido}

    def serial_get(self, valor):
        self.JSON_dict["valor"] = valor
        self.valor = valor


class IPPublica(RestApiComunicacion):

    def __init__(self):
        self.id = 1
        self.ip_publica = "1.0.0.0"
        self.JSON_dict = {"id": self.id, "ip": self.ip_publica}

    def api_get(self, token):

        peticion = super().api_get(endpoint=API_ENDPOINT_IP_NOW,
                                   token=token)
        self.ip_publica = peticion["request_dict"]["ip"]
        self.JSON_dict["id"] = 1
        self.JSON_dict["ip"] = self.ip_publica

    def api_put(self, token):
        peticion = super().api_put(endpoint=API_ENDPOINT_IP_LAST,
                                   token=token,
                                   data=self.JSON_dict)
        respuesta = peticion["respuesta"]
        if respuesta is not None:
            if super().api_check_respuesta(respuesta) == 1:
                acceder_requerido = False
            elif super().api_check_respuesta(respuesta) == 2:
                acceder_requerido = True
        else:
            acceder_requerido = False

        return {"acceder_req": acceder_requerido}


class Alarma(RestApiComunicacion):

    def __init__(self, tipo, mensaje):
        self.tipo = tipo  # 3-> AGUA, 4-> GAS, 5-> PIR
        self.mensaje = mensaje  # 2-> AGUA, 1-> GAS, 3-> PIR
        self.valor = False
        self.fecha = datetime
        self.JSON_dict = {
            "tipo_id": self.tipo, "mensaje_id": self.mensaje}

        self.tiempo_ini = None
        self.incremento = None

    def api_post(self, token):

        peticion = super().api_post(endpoint=API_ENDPOINT_ALARMAS,
                                    token=token,
                                    data=self.JSON_dict)
        respuesta = peticion["respuesta"]
        print("Envío de alarma")
        if respuesta is not None:
            if super().api_check_respuesta(respuesta) == 1:
                acceder_requerido = False
                self.valor = False
            elif super().api_check_respuesta(respuesta) == 2:
                acceder_requerido = True
        else:
            acceder_requerido = False
        return {"acceder_req": acceder_requerido}

    def serial_get(self, valor):
        self.valor = valor


class Sidocs(RestApiAutenticacion):

    def __init__(self):
        # Intentos de conexión serial
        self.intentos = 0
        # Se crea un objeto para establecer la conexión serie:
        # Indicando port=None en la creación del objeto "Serial", no se ejecuta el método open() y debe ser llamado explicitamente
        self.arduino = Serial(port=None,
                              baudrate=ARDUINO_PARAMS["BAUD_RATE"],
                              timeout=ARDUINO_PARAMS["TIEMPO_ESPERA"])
        # Conexión serie establecida
        self.arduino_conectado = False
        # Conexión serie sincronizada con Arduino
        self.arduino_sincronizado = False
        # Token de autenticación JWT
        self.token = None
        # Atributos auxiliares para detectar la necesidad de refrescar el token JWT o
        # reautenticar al usuario en el servidor REST.
        self.acceder_requerido = False
        # Objeto modo de funcionamiento
        self.modo_funcionamiento = ModoFuncionamiento()
        # Atributo auxiliar para detectar un cambio en el modo de funcionamiento
        self.cambio_modo = False
        # Objeto Temperatura y Humedad
        self.temperatura = Temperatura()
        self.humedad = Humedad()
        self.ip = IPPublica()
        # Objetos alarmas, inicializadas con su correspondiente tipo y mensaje asociado
        self.alarma_agua = Alarma(tipo=3, mensaje=5)
        self.alarma_gas = Alarma(tipo=4, mensaje=4)
        self.alarma_pir = Alarma(tipo=5, mensaje=6)
        # Atributos auxiliares para el control de envío de alarmas al servidor REST por tiempo
        self.tiempo_ini_agua = None
        self.tiempo_ini_gas = None
        self.tiempo_ini_pir = None
        self.incremento_agua = None
        self.incremento_gas = None
        self.incremento_pir = None
        # Cadena de caracteres leída a través del puerto serie
        self.lectura_str = None
        self.valores = []
        # Carácter inicial de la cadena # P -> Envío de Parámetros,
                                            # M -> Envío de Modo de Funcionamiento,
                                        # D -> Recepción de datos,
                                            # A -> Alarmas
                                            # T -> Temperatura
                                            # H -> Humedad
                                        # S -> Sincronización
        self.caracter_inicial = None
        # Cadena de caracteres con los parámetros a enviar a Arduino por puerto serie
        self.parametros = None


    #Autenticación y obtención del token de comunicación con REST API
    def api_acceder(self):

        peticion = super().api_acceder(usuario=REST_API_PARAMS["USUARIO"],
                                     password=REST_API_PARAMS["CONTRASENA"],
                                     endpoint=API_ENDPOINT_AUTH)

        # Se guarda y evalúa la respuesta del servidor
        respuesta = peticion["respuesta"]

        if respuesta is not None:

            if super().api_check_respuesta(respuesta) == 1:

                # La petición ha sido correcta y por tanto se almacena el nuevo token JWT
                self.token = peticion["token"]
                self.acceder_requerido = False
                print("Acceso Efectuado")

        # La petición no se ha ejecutado correctamente
        else:
            self.acceder_requerido = True
            log.logger.info("No se ha podido autenticar el cliente RPi \r\n {0}")
            print("Acceso Requerido")
        return


    # Renovación de token
    def api_refrescar_token(self):

        peticion = super().api_refrescar(endpoint=API_ENDPOINT_REFRESH,
                                       token=self.token)

        # Se guarda y evalúa la respuesta del servidor
        respuesta = peticion["respuesta"]

        if respuesta is not None:

            # La petición ha sido correcta y por tanto se almacena el nuevo token JWT
            if super().api_check_respuesta(respuesta) == 1:

                # La petición ha sido correcta y por tanto se almacena el nuevo token JWT
                self.token = peticion["token"]
                self.acceder_requerido = False
                log.logger.info("Refresco de token JWT efectuado \r\n {0}")
                print("Refresco de token efectuado")

            # La petición se ha ejecutado correctamente pero se requiere una nueva autenticación.
            elif super().api_check_respuesta(respuesta) == 3:

                self.acceder_requerido = True
                log.logger.info("Fallo en el refresco de token JWT. Se requiere una nueva autenticación \r\n {0}")
                print("Acceso requerido tras intento de refresco de token")

        # La petición no se ha ejecutado correctamente
        else:
            self.acceder_requerido = True
            log.logger.info("No se ha podido refrescar el token JWT \r\n {0}")
        return


    # Recuperar el modo de funcionamiento actual
    def api_parametros_obtener(self):

        peticion = self.modo_funcionamiento.api_get(token=self.token)
        respuesta = peticion["acceder_req"]
        self.acceder_requerido = respuesta
        self.cambio_modo = peticion["cambio"]
        if respuesta:
            return


    # Recuperar la ip publica actual
    def api_ip_obtener(self):

        peticion = self.ip.api_get(token=None)


    # Actualización de la ip pública
    def api_ip_actualizar(self):

        peticion = self.ip.api_put(token=self.token)
        respuesta = peticion["acceder_req"]
        self.acceder_requerido = respuesta
        if respuesta:
            return

    # Envío de datos de Temperatura, Humedad y las Alarmas (de existir) a API REST 
    def api_datos_enviar(self):

        p_temp = self.temperatura.api_post(token=self.token)

        # Obtenemos el resultado de la petición POST
        r_temp = p_temp["acceder_req"]
        self.acceder_requerido = r_temp
        if self.acceder_requerido:
            return

        p_hum = self.humedad.api_post(token=self.token)

        # Obtenemos el resultado de la petición POST
        r_hum = p_hum["acceder_req"]
        self.acceder_requerido = r_hum
        if self.acceder_requerido:
            return

        # Consultamos si hay una alarma por detección de inundación
        if self.alarma_agua.valor:

            if self.tiempo_ini_agua is not None:
                delta_agua = datetime.now() - self.tiempo_ini_agua
                self.incremento_agua = delta_agua.seconds

            if self.incremento_agua is None or self.incremento_agua > SYSTEM_PARAMS["BLOQUEO_ALARMA"]:
                p_alarm_agua = self.alarma_agua.api_post(token=self.token)
                print("Alarma de agua enviada")
                log.logger.info("Alarma de agua enviada \r\n {0}")

                # Consultamos el resultado de la petición POST.
                self.acceder_requerido = p_alarm_agua["acceder_req"]
                self.tiempo_ini_agua = datetime.now()

                # Consultamos si se requiere la ejecución de un nuevo acceso o no.
                if self.acceder_requerido:
                    # Si fuese necesario ejecutar de nuevo el proceso de acceso,
                    # retornamos al hilo de ejecución principal
                    return

        # Consultamos si hay una alarma por detección de gas
        if self.alarma_gas.valor:

            if self.tiempo_ini_gas is not None:
                delta_gas = datetime.now() - self.tiempo_ini_gas
                self.incremento_gas = delta_gas.seconds

            if self.incremento_gas is None or self.incremento_gas > SYSTEM_PARAMS["BLOQUEO_ALARMA"]:
                p_alarm_gas = self.alarma_gas.api_post(token=self.token)
                print("Alarma de gas enviada")
                log.logger.info("Alarma de gas enviada \r\n {0}")

                # Consultamos el resultado de la petición POST.
                self.acceder_requerido = p_alarm_gas["acceder_req"]
                self.tiempo_ini_gas = datetime.now()

                # Consultamos si se requiere la ejecución de un nuevo acceso o no.
                if self.acceder_requerido:
                    # Si fuese necesario ejecutar de nuevo el proceso de acceso,
                    # retornamos al hilo de ejecución principal
                    return

        # Consultamos si hay una alarma por detección de movimiento
        if self.alarma_pir.valor:

            if self.tiempo_ini_pir is not None:
                delta_pir = datetime.now() - self.tiempo_ini_pir
                self.incremento_pir = delta_pir.seconds

            if self.incremento_pir is None or self.incremento_pir > SYSTEM_PARAMS["BLOQUEO_ALARMA"]:
                p_alarm_pir = self.alarma_pir.api_post(token=self.token)
                print("Alarma de Movimiento enviada")
                log.logger.info("Alarma de Movimiento enviada \r\n {0}")

                # Consultamos el resultado de la petición POST.
                self.acceder_requerido = p_alarm_pir["acceder_req"]
                self.tiempo_ini_pir = datetime.now()

                # Consultamos si se requiere la ejecución de un nuevo acceso o no.
                if self.acceder_requerido:
                    # En caso de ser necesario ejecutar de nuevo el proceso de acceso,
                    # retornamos al hilo principal.
                    return


    # Establecimiento de la conexión con Arduino por puerto serie
    def serial_conectar(self):

        self.arduino.port = ARDUINO_PARAMS["PUERTO_SERIAL"]
        self.arduino.open()
        if self.arduino.is_open:
            self.arduino_conectado = True
            self.arduino.dtr = False
            sleep(1)
            self.arduino.flushInput()
            self.arduino.dtr = True

        return self.arduino_conectado


    # Desconexión de puerto serie con Arduino
    def serial_desconectar(self):

        self.arduino_conectado = False
        return self.arduino.close()


    def serial_sincronizar(self):
        try:
            lectura = self.arduino.readline()
            self.lectura_str = str(lectura[0:len(lectura) - 2].decode("utf-8"))
            if self.lectura_str == 'S':
                self.arduino_sincronizado = True
                print("Sincronizada la conexión con Arduino")
                log.logger.info("Éxito al sincronizar la conexión con Arduino \r\n {0}")
            else:
                self.arduino_sincronizado = False
                print("Sincronizando conexión con Arduino...")
                log.logger.info("Sincronizando conexión con Arduino... \r\n {0}")
        except SerialException as e:
            self.arduino_sincronizado = False
            print("Error al intentar sincronizar la conexión con Arduino")
            log.logger.exception("Error al intentar sincronizar la conexión con Arduino \r\n {0}".format(e))
        return self.arduino_sincronizado

    # Obtención de valores de Temperatura, Humedad y estado de Alarmas por puerto serie
    def serial_datos_obtener(self):

        # Se solicita a Arduino la recepción de datos (Alarmas, Temperatura y Humedad)
        self.caracter_inicial = 'D'
        self.arduino.write(self.caracter_inicial.encode("utf-8"))

        # Se leen los datos enviados por Arduino y se almacenan en una lista, aplicando el siguiente formato:
        # [T,tt.tt,H,hh.hh,A,aPir,aGas,aAgua]
        lectura = self.arduino.readline()
        self.lectura_str = str(lectura[0:len(lectura) - 2].decode("utf-8"))

        # Se almacenan la cadena de caracteres leída, en una lista
        self.valores = self.lectura_str.split(CARACTER_SEPARACION)
        longitud = len(self.valores)
        print(self.valores)

        # Se controla que la longitud de la lista de datos leída sea correcta. (Longitud 8)
        if longitud == 9 and self.valores[longitud-1] == '$':
            for i in range(longitud):

                if self.valores[i] == 'T':
                    self.temperatura.serial_get(float(self.valores[i+1]))
                elif self.valores[i] == 'H':
                    self.humedad.serial_get(float(self.valores[i+1]))
                elif self.valores[i] == 'A':
                    self.alarma_pir.serial_get(bool(strtobool(self.valores[i+1])))
                    self.alarma_gas.serial_get(bool(strtobool(self.valores[i+2])))
                    self.alarma_agua.serial_get(bool(strtobool(self.valores[i+3])))

            return True
        else:
            return False

    # Envío de parámetros (Modo de Funcionamiento) por puerto serie
    def serial_parametros_enviar(self):

        self.caracter_inicial = 'P'
        self.parametros = self.caracter_inicial + \
                          CARACTER_SEPARACION + \
                          self.modo_funcionamiento.serial_set() + \
                          CARACTER_FIN_CADENA
        self.arduino.write(self.parametros.encode("utf-8"))
        print("Cadena enviada: ", self.parametros)
