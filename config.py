
REST_API_PARAMS = {
        "ROOT_URL": "https://ccmanzanero.pythonanywhere.com",
        "USUARIO": "ccmanzanero",
        "CONTRASENA": "carlos21"
    }

ARDUINO_PARAMS = {
        "INTENTOS_CONEXION_SERIAL": 3,
        "PUERTO_SERIAL": "/dev/ttyACM0",
        "BAUD_RATE": 9600,
        "TIEMPO_ESPERA": 5
    }

SYSTEM_PARAMS = {

        "RUTA_LOGS": "logs/sidocs_logfile.log",
        # Se bloquea el nuevo envío de alarmas de tipo N a BBDD transcurridos # segundos del envío inicial
        "BLOQUEO_ALARMA": 60,
        "TIEMPO_REFRESCO_TOKEN": 600 # 10 minutos
    }