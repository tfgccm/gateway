import requests
import json
import log
from serial import *
from time import *

RES_OK = 200
RES_CREADO = 201
RES_PETICION_INCORRECTA = 400
RES_NO_AUTORIZADO = 401


class RestApiAutenticacion():

    def api_acceder(self, usuario, password, endpoint):

        # Objeto JSON para el proceso de autenticación
        credenciales_json = {
            "username": usuario,
            "password": password
        }
        # Se realiza la petición POST al servidor indicando la URL y el Cuerpo
        peticion = requests.post(url=endpoint,
                                 data=credenciales_json)
        # Se guarda el código de respuesta
        respuesta = peticion.status_code
        try:
            # Se guarda la el token JWT obtenido de la respuesta del servidor
            token = peticion.json()["token"]
        except Exception as e:
            log.logger.exception("Fallo al intentar autenticar al usuario " + str(usuario) + " \r\n {0}".format(e))
            token = None
        # Se retorna el token JWT y el código de la respuesta del servidor
        return {"token": token, "respuesta": respuesta}


    def api_refrescar(self, endpoint, token):

        # Objeto JSON para el proceso de refresco de token JWT
        refresh_json = {
            "token": token
        }
        # Se realiza la petición POST al servidor indicando la URL y el Cuerpo
        peticion = requests.post(url=endpoint,
                                 data=refresh_json)
        # Se guarda el código de respuesta
        respuesta = peticion.status_code
        try:
            # Se guarda la el token JWT obtenido de la respuesta del servidor
            token = peticion.json()["token"]
        except Exception as e:
            log.logger.exception("Fallo al intentar refrescar el token, se requiere una nueva autenticación \r\n {0}".format(e))
            token = None
        # Se retorna el token JWT y el código de la respuesta del servidor
        return {"token": token, "respuesta": respuesta}


    # Método para comprobar y tipar el resultado de las peticiones al servidor
    def api_check_respuesta(self, respuesta):

        # Petición correcta
        if respuesta in (RES_OK, RES_CREADO):
            return 1
        # Requerido refrescar token
        elif respuesta == RES_NO_AUTORIZADO:
            return 2
        # Requerido acceso
        elif respuesta == RES_PETICION_INCORRECTA:
            return 3


class RestApiComunicacion():

    # Método para ejecutar peticiones GET al servidor
    def api_get(self, endpoint, token):

        # Se controla si la petición requiere una autenticación por token JWT en la cabecera
        if token is None:

            token_header = None

        else:
            token_header = {
                "Authorization": "JWT " + token
            }
        try:
            # Se realiza la petición GET al servidor indicando la URL y la Cabecera
            peticion = requests.get(url=endpoint,
                                    headers=token_header)
            # Se guarda el código de respuesta
            respuesta = peticion.status_code
            # Se guarda la respuesta del servidor
            peticion_dict = json.loads(peticion.text)
        except Exception as e:
            log.logger.exception("Fallo al intentar ejecutar la petición GET \r\n {0}".format(e))
            respuesta = None
            peticion_dict = None
        # Se retorna el valor y el código de la respuesta del servidor
        return {"request_dict": peticion_dict, "respuesta": respuesta}


    # Método para ejecutar peticiones POST al servidor
    def api_post(self, endpoint, token, data):

        # Se controla si la petición requiere una autenticación por token JWT en la cabecera
        if token is None:

            token_header = None

        else:
            token_header = {
                "Authorization": "JWT " + token
            }
        try:
            # Se realiza la petición POST al servidor indicando la URL, la Cabecera y el Cuerpo
            peticion = requests.post(url=endpoint,
                                     headers=token_header,
                                     data=data)
            # Se guarda la respuesta del servidor
            respuesta = peticion.status_code
            # Se guarda la respuesta del servidor
            peticion_dict = json.loads(peticion.text)
        except Exception as e:
            log.logger.exception("Fallo al intentar ejecutar la petición POST \r\n {0}".format(e))
            respuesta = None
            peticion_dict = None
        # Se retorna el valor y el código de la respuesta del servidor
        return {"request_dict": peticion_dict, "respuesta": respuesta}


    # Método para ejecutar peticiones PUT al servidor
    def api_put(self, endpoint, token, data):

        # Se controla si la petición requiere una autenticación por token JWT en la cabecera
        if token is None:

            token_header = None

        else:
            token_header = {
                "Authorization": "JWT " + token
            }
        try:
            # Se realiza la petición POST al servidor indicando la URL, la Cabecera y el Cuerpo
            peticion = requests.put(url=endpoint,
                                    headers=token_header,
                                    data=data)
            # Se guarda la respuesta del servidor
            respuesta = peticion.status_code
            # Se guarda la respuesta del servidor
            peticion_dict = json.loads(peticion.text)
        except Exception as e:
            log.logger.exception("Fallo al intentar ejecutar la petición PUT \r\n {0}".format(e))
            respuesta = None
            peticion_dict = None
        # Se retorna el valor y el código de la respuesta del servidor
        return {"request_dict": peticion_dict, "respuesta": respuesta}

    # Método para comprobar y tipar el resultado de las peticiones al servidor
    def api_check_respuesta(self, respuesta):

        # Petición correcta
        if respuesta in (RES_OK, RES_CREADO):
            return 1
        # Requerido refrescar token
        elif respuesta == RES_NO_AUTORIZADO:
            return 2
        # Requerido acceso
        elif respuesta == RES_PETICION_INCORRECTA:
            return 3


