import logging
import serial
from config import ARDUINO_PARAMS, SYSTEM_PARAMS
from models import Sidocs
import log
import threading
import time



def thread_arduino_restapi():

    while sidocs.intentos < ARDUINO_PARAMS["INTENTOS_CONEXION_SERIAL"]:

        try:
            # Se inicia la conexión serial
            log.logger.info("Conectando con Arduino...")
            if sidocs.serial_conectar():
                log.logger.info("Conexión establecida con Arduino")
                break

        except serial.SerialException as e:
            sidocs.intentos += 1
            log.logger.exception("Error al intentar establecer la conexión con Arduino en su intento " + str(
                sidocs.intentos) + " \r\n {0}".format(e))
            if sidocs.intentos == 3:
                sidocs.serial_desconectar()
                log.logger.info("Cerrando la conexión con Arduino")

    # Mientras la conexión serie con Arduino esté establecida
    while sidocs.arduino_conectado:

        # Se sincroniza la comunicación con Arduino
        while sidocs.arduino_sincronizado is False:
            if sidocs.serial_sincronizar():
                break

        # Se evaluan los criterios necesarios para establecer el flujo de lectura/escritura con Arduino
        if sidocs.token is not None and \
                sidocs.acceder_requerido is False and \
                sidocs.arduino_sincronizado is True:

            # Se obtienen los parámetros de funcionamiento actuales del servidor (Modo de Funcionamiento)
            sidocs.api_parametros_obtener()
            # Se comprueba si ha habido un cambio de modo de funcionamiento
            if sidocs.cambio_modo:
                # Se envía los nuevos parámetros de funcionamiento a Arduino
                sidocs.serial_parametros_enviar()
                #time.sleep(1.5)
            # Se obtienen los datos de temperatura, humedad y estado de alarmas de Arduino
            elif sidocs.serial_datos_obtener():
                # Se envían y guardan los datos obtenidos en el servidor REST
                sidocs.api_datos_enviar()
            else:
                pass
            time.sleep(2.5)


def thread_actualizar_ip():

    while 1:

        if sidocs.token is not None and \
                sidocs.acceder_requerido is False:
            # Se obtiene la IP pública actual
            sidocs.api_ip_obtener()
            # Se actualiza su valor en el servidor REST
            sidocs.api_ip_actualizar()
            time.sleep(180)  # 3600 s = 1 hora


def thread_api_acceder():

    while 1:
        # Si el token JWT es nulo y por lo tanto es la primera vez que se ejecuta el proceso de acceso o
        # si se requiere efectuar de nuevo un proceso de acceso
        if sidocs.token is None or \
                sidocs.acceder_requerido:
            # Se ejecuta la petición de acceso al servidor REST para obtener y guardar el token JWT
            sidocs.api_acceder()


def thread_api_refresco_token():

    while 1:

        # Se ejecuta de manera periódica un refresco del token JWT cada 10 minutos
        if sidocs.token is not None:
            time.sleep(SYSTEM_PARAMS["TIEMPO_REFRESCO_TOKEN"])     # 10 minutos
            sidocs.api_refrescar_token()

def main():

    a = threading.Thread(target=thread_arduino_restapi)
    b = threading.Thread(target=thread_actualizar_ip)
    c = threading.Thread(target=thread_api_acceder)
    d = threading.Thread(target=thread_api_refresco_token)

    a.start()
    b.start()
    c.start()
    d.start()


if __name__ == "__main__":
    # Creamos el objeto sidocs
    sidocs = Sidocs()
    # Invocamos el método main
    main()