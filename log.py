import logging
from logging import handlers
from config import SYSTEM_PARAMS

# Se crea el objeto logger
logger = logging.getLogger('sidocs_logger')
logger.setLevel(logging.INFO)

# Se crea el archivo handler que registrará los mensajes y se fuerza que se genere un archivo nuevo cada hora,
# limitando el número de archivos a 10.
h = logging.handlers.TimedRotatingFileHandler(SYSTEM_PARAMS["RUTA_LOGS"], when='H', interval=1, backupCount=20)
h.setLevel(logging.INFO)

# Se crea el objeto Formatter que dará formato a nuestros mensajes: 'aaa-mm-dd hh:mm:ss,ms Mensaje'
formatter = logging.Formatter('%(asctime)s %(message)s')
h.setFormatter(formatter)

# Se añade el handler al logger
logger.addHandler(h)
